function half
a=0;b=10;
e=0.00001;
[y,x]=minimum(a,b,e);
y
x
wh=a:0.01:b;
figure(1);
plot(wh,f(wh),'g');hold on;
plot(x,f(x),'*');
figure(2);
plot(f(x),'*-');
end

function [y,xx]=minimum(startPoint,endPoint,eps)
xx=[];
a=startPoint;
b=endPoint;
i=1;
while(b-a>eps)
c=(a+b)/2;
F=f(c);
if(f(a)<F)
    b=c;
    xx(i)=b;
    i=i+1;
else
    if(F<f(b))
        a=c;
        xx(i)=a;
        i=i+1;
    else
        a=(a+c)/2;
        xx(i)=a;
        b=(b+c)/2;
        xx(i+1)=b;
        i=i+2;
    end
end
y=F;
end
end

function y=f(x)
y=sin(x);
end